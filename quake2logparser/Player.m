//
//  Player.m
//  quake2logparser
//
//  Created by Michael Costa on 2/8/13.
//  Copyright (c) 2013 Michael Costa. All rights reserved.
//

#import "Player.h"
#import "NSString+Trimming.h"

@interface Player () {
    NSInteger currentKillStreak;
}

@property (nonatomic, readwrite) NSInteger hotStreak;

@end

@implementation Player

- (id)init
{
    self = [super init];
    if (self) {
        self.victimList = [NSMutableArray array];
        self.enemyList = [NSMutableArray array];
        self.hotStreak = 0;
        currentKillStreak = 0;
    }
    return self;
}

- (NSInteger)score
{
    NSInteger teamCapScore = [self.team teamCaps] * 10;
    return (self.kills + (self.caps * 5) + (self.defendsFlagCarrier * 2) + (self.defendsFlag * 2) + (self.defendsAggressive * 3) + self.defendsBase + self.assists + self.returns + (self.efckills * 2) - self.suicides - self.deaths + teamCapScore);
}

- (NSInteger)ctfScore
{
    return (self.kills + (self.caps * 5) + (self.defendsFlagCarrier * 2) + (self.defendsFlag * 2) + (self.defendsAggressive * 3) + self.defendsBase + self.assists + self.returns + (self.efckills * 2) - self.suicides - self.deaths);
}

- (NSInteger)maxHotStreak
{
    if (self.killed == 0) {
        return currentKillStreak;
    }
    if (currentKillStreak > self.hotStreak) {
        return currentKillStreak;
    }
    return self.hotStreak;
}

- (CGFloat)kdratio
{
    return ((CGFloat)_kills/(CGFloat)_killed);
}

- (NSString *)teamName
{
    if (self.isRedTeam) {
        return @"Red";
    } else {
        return @"Blue";
    }
}

- (NSInteger)assists
{
    return (_assistDefend + _assistKill + _assistReturn);
}

- (NSInteger)defends
{
    return (_defendsAggressive + _defendsBase + _defendsFlag + _defendsFlagCarrier);
}

- (NSInteger)returns
{
    return  (_returnsFlag + _returnsHelped);
}

- (void)addVictim:(NSString *)victim
{
    [self.victimList addObject:victim];
    currentKillStreak++;
}

- (void)addEnemy:(NSString *)enemy
{
    [self.enemyList addObject:enemy];
    if (currentKillStreak > self.hotStreak) {
        self.hotStreak = currentKillStreak;
    }
    currentKillStreak = 0;
}

- (NSString *)worstEnemy
{
    return [self findMostOccurring:self.enemyList];
}

- (NSString *)easiestVictim
{
    return [self findMostOccurring:self.victimList];
}

- (NSString *)findMostOccurring:(NSArray *)list
{
    if ([list count] == 0) {
        return @"--";
    }
    NSCountedSet *bag = [[NSCountedSet alloc] initWithArray:list];
    NSString *mostOccurring;
    NSUInteger highest = 0;
    for (NSString *s in bag)
    {
        if ([bag countForObject:s] > highest)
        {
            highest = [bag countForObject:s];
            mostOccurring = s;
        }
    }
    return [NSString stringWithFormat:@"%@ (%d)", mostOccurring, highest];
}

- (NSArray *)sortedKillArray
{
    NSCountedSet *countedSet = [[NSCountedSet alloc] initWithArray:self.victimList];
    NSArray *distinctArray = [[countedSet allObjects] sortedArrayUsingComparator:^(id a, id b) {
        NSUInteger obj1Count = [countedSet countForObject:a];
        NSUInteger obj2Count = [countedSet countForObject:b];
        
        if (obj1Count > obj2Count) return NSOrderedAscending;
        else if (obj1Count < obj2Count) return NSOrderedDescending;
        return NSOrderedSame;
    }];
    return distinctArray;
}

- (NSInteger)killsForName:(NSString *)name
{
    NSCountedSet *countedSet = [[NSCountedSet alloc] initWithArray:self.victimList];
    return [countedSet countForObject:name];
}

- (NSInteger)killedByName:(NSString *)name
{
    NSCountedSet *countedSet = [[NSCountedSet alloc] initWithArray:self.enemyList];
    return [countedSet countForObject:name];
}

- (NSString *)printCTFInfoLine
{
    NSString *info = [NSString stringWithFormat:@"<tr><td>%@</td>"
                      @"<td>%@</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td></tr>",
                      [self.name fixName],
                      self.teamName,
                      self.caps,
                      self.assists,
                      self.returns,
                      self.defends,
                      self.efckills,
                      self.steals,
                      self.drops,
                      self.score
                      ];
    return info;
}

- (NSString *)printKillsLine
{
    NSString *info = [NSString stringWithFormat:@"<tr><td>%@</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%0.2f</td>"
                      @"<td>%@</td>"
                      @"<td>%@</td>"
                      @"<td>%d</td>"
                      @"</tr>",
                      [self.name fixName],
                      self.kills,
                      self.killed,
                      self.suicides,
                      self.deaths,
                      self.kdratio,
                      [self worstEnemy],
                      [self easiestVictim],
                      self.hotStreak
                      ];
    return info;
}

- (NSString *)printWeaponStats
{
    NSString *info = [NSString stringWithFormat:@"<tr><td>%@</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td></tr>",
                      [self.name fixName],
                      self.blaster,
                      self.shotgun,
                      self.supershotgun,
                      self.machinegun,
                      self.chaingun,
                      self.grenadelauncher,
                      self.rocketlauncher,
                      self.hyperblaster,
                      self.railgun,
                      self.bfg,
                      self.grenade,
                      self.telefrag
                      ];
    return info;
}

- (NSString *)description
{
    NSString *desc = @"";
    desc = [desc stringByAppendingFormat:@"Player:\n\t name: %@,"
            "\n\t isRedTeam: %d,"
            "\n\t caps: %d,"
            "\n\t assists: %d,"
            "\n\t returns: %d,"
            "\n\t defends: %d,"
            "\n\t efckills: %d,"
            "\n\t steals: %d,"
            "\n\t drops: %d,"
            "\n\t kills: %d,"
            "\n\t killed: %d,"
            "\n\t suicides: %d,"
            "\n\t deaths: %d,"
            "\n\t blaster: %d,"
            "\n\t shotgun: %d,"
            "\n\t supershotgun: %d,"
            "\n\t machinegun: %d,"
            "\n\t chaingun: %d,"
            "\n\t grenadelauncher: %d,"
            "\n\t rocketlancher: %d,"
            "\n\t hyperblaster: %d,"
            "\n\t railgun: %d,"
            "\n\t bfg: %d,"
            "\n\t grenade: %d,"
            "\n\t telefrag: %d,"
            "\n\t teamName: %@,"
            "\n\t team: %@,"
            "\n\t assistDefend: %d,"
            "\n\t assistKill: %d,"
            "\n\t assistReturn: %d,"
            "\n\t returnFlag: %d,"
            "\n\t returnHelped: %d,"
            "\n\t defendAgg: %d,"
            "\n\t defendFlag: %d,"
            "\n\t defendBase: %d,"
            "\n\t defendFC: %d,"
            "\n\t score: %d,",
            self.name,
            self.isRedTeam,
            self.caps,
            self.assists,
            self.returns,
            self.defends,
            self.efckills,
            self.steals,
            self.drops,
            self.kills,
            self.killed,
            self.suicides,
            self.deaths,
            self.blaster,
            self.shotgun,
            self.supershotgun,
            self.machinegun,
            self.chaingun,
            self.grenadelauncher,
            self.rocketlauncher,
            self.hyperblaster,
            self.railgun,
            self.bfg,
            self.grenade,
            self.telefrag,
            self.teamName,
            self.team,
            self.assistDefend,
            self.assistKill,
            self.assistReturn,
            self.returnsFlag,
            self.returnsHelped,
            self.defendsAggressive,
            self.defendsFlag,
            self.defendsBase,
            self.defendsFlagCarrier,
            self.score
            ];
    return desc;
}

@end
