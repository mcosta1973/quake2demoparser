//
//  ViewController.m
//  quake2logparser
//
//  Created by Michael Costa on 2/8/13.
//  Copyright (c) 2013 Michael Costa. All rights reserved.
//

#import "ViewController.h"
#import "NSString+Trimming.h"

#define LFIRE                                           0

#define BLASTER_KILL                                    @"was blasted by"
#define SHOTGUN_KILL                                    @"was gunned down by"
#define SSG_KILL                                        @"was blown away by"
#define MACHINEGUN_KILL                                 @"was machinegunned by"
#define CHAINGUN_KILL                                   @"was cut in half by"
#define GRENADE_LAUNCHER_KILL_1                         @"was popped by"
#define GRENADE_LAUNCHER_KILL_2                         @"was shredded by"
#define ROCKET_KILL_1                                   @" ate "
#define ROCKET_KILL_2                                   @"almost dodged"
#define HYPERBLASTER_KILL                               @"was melted by"
#define RAILGUN_KILL                                    @"was railed by"
#define BFG_KILL_1                                      @"saw the pretty lights from"
#define BFG_KILL_2                                      @"was disintegrated by"
#define BFG_KILL_3                                      @"couldn't hide from"
#define GRENADE_KILL_1                                  @"caught"
#define GRENADE_KILL_2                                  @"didn't see"
#define GRENADE_KILL_3                                  @"feels"
#define TELEFRAG_KILL                                   @"tried to invade"
#define FLAG_STEAL                                      @"stole"
#define FLAG_STEAL_LFIRE                                @"got the"
#define FLAG_CAPTURE                                    @"captured"
#define ASSIST                                          @"assisted"
#define ASSIST_DEFEND                                   @"against an aggressive enemy"
#define ASSIST_FRAG                                     @"gets an assist for fragging the flag carrier!"
#define ASSIST_RETURN                                   @"gets an assist for returning the flag!"
#define FLAG_DROP                                       @"lost the"
#define EFC_KILL                                        @"killed the enemy flag carrier"
#define EFC_KILL_LFIRE                                  @"BONUS"
#define DEFEND                                          @"defends"
#define RETURN_HELP                                     @"helped"
#define RETURN                                          @"returned"
#define SUICIDE_GRENADE_1                               @"tried to put the pin back in"
#define SUICIDE_GRENADE_2                               @"tripped on"
#define SUICIDE_ROCKET                                  @"blew"
#define SUICIDE_BFG                                     @"should have used a smaller gun"
#define SUICIDE_KILL                                    @"killed"
#define SUICIDE_DEFAULT                                 @"suicides"
#define DEATH_FALLING                                   @"cratered"
#define DEATH_SQUISH                                    @"was squished"
#define DEATH_SANK                                      @"sank like a rock"
#define DEATH_SLIME                                     @"melted"
#define DEATH_LAVA                                      @"does a back flip into the lava"
#define DEATH_BARREL                                    @"blew up"
#define DEATH_LASER                                     @"saw the light"
#define DEATH_HURT                                      @"was in the wrong place"

#define MAP_NAME                                        @"s5-r2-dfvldr-lm31-jab"

@interface ViewController () {
    int kills;
    int deaths;
    NSString *previousKiller;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.phraseArray = @[BLASTER_KILL,
                         SHOTGUN_KILL,
                         SSG_KILL,
                         MACHINEGUN_KILL,
                         CHAINGUN_KILL,
                         GRENADE_LAUNCHER_KILL_1,
                         GRENADE_LAUNCHER_KILL_2,
                         ROCKET_KILL_1,
                         ROCKET_KILL_2,
                         HYPERBLASTER_KILL,
                         RAILGUN_KILL,
                         BFG_KILL_1,
                         BFG_KILL_2,
                         BFG_KILL_3,
                         GRENADE_KILL_1,
                         GRENADE_KILL_2,
                         GRENADE_KILL_3,
                         TELEFRAG_KILL,
                         FLAG_CAPTURE,
                         FLAG_DROP,
                         FLAG_STEAL,
                         FLAG_STEAL_LFIRE,
                         ASSIST,
                         ASSIST_FRAG,
                         ASSIST_DEFEND,
                         ASSIST_RETURN,
                         DEFEND,
                         RETURN,
                         RETURN_HELP,
                         EFC_KILL,
                         EFC_KILL_LFIRE,
                         SUICIDE_BFG,
                         SUICIDE_DEFAULT,
                         SUICIDE_GRENADE_1,
                         SUICIDE_GRENADE_2,
                         SUICIDE_KILL,
                         SUICIDE_ROCKET,
                         DEATH_BARREL,
                         DEATH_FALLING,
                         DEATH_HURT,
                         DEATH_LASER,
                         DEATH_LAVA,
                         DEATH_SANK,
                         DEATH_SLIME,
                         DEATH_SQUISH
                         ];
    self.blueTeam = [[Team alloc] init];
    self.redTeam = [[Team alloc] init];
    self.blueTeam.teamColor = TeamTypeBlue;
    self.redTeam.teamColor = TeamTypeRed;
    self.playerArray = [NSMutableArray array];
    kills = 0;
    deaths = 0;
    self.mapName = MAP_NAME;
    [self loadFile];
    [self printPlayers];
}

- (NSData*)readDataFromFileAtURL:(NSURL*)anURL {
    NSFileHandle* aHandle = [NSFileHandle fileHandleForReadingFromURL:anURL error:nil];
    NSData* fileContents = nil;
    if (aHandle)
        fileContents = [aHandle readDataToEndOfFile];
    return fileContents;
}

- (NSString *)hexStringFromData:(NSData *)data
{
    NSUInteger capacity = [data length] * 2;
    NSMutableString *stringBuffer = [NSMutableString stringWithCapacity:capacity];
    const unsigned char *dataBuffer = [data bytes];
    NSInteger i;
    char x;
    for (i=0; i<[data length]; ++i) {
//        DebugLog(@"%d: %c", ((NSUInteger)dataBuffer[i] & 0xFF), ((NSUInteger)dataBuffer[i] & 0xFF));
        if (((NSUInteger)dataBuffer[i] & 0xFF) > 127) {
            x = ((NSUInteger)dataBuffer[i] & 0xFF) - 128;
        } else {
            x = ((NSUInteger)dataBuffer[i] & 0xFF);
        }
//        [stringBuffer appendFormat:@"%c",(char)((NSUInteger)dataBuffer[i] & 0xFF)];
        [stringBuffer appendFormat:@"%c", x];
    }
    return stringBuffer;
}

- (void)loadFile
{
//    NSString *path = [[NSBundle mainBundle] pathForResource: @"qconsole" ofType: @"log"];
    NSString *path = [[NSBundle mainBundle] pathForResource:MAP_NAME ofType: @"dm2"];
    NSURL *url = [NSURL fileURLWithPath:path];
    DebugLog(@"url: %@", url);
    NSData *data = [self readDataFromFileAtURL:url];
    NSString *string = [self hexStringFromData:data];
    [self parseFile:string];
/*
    if ([[NSFileManager defaultManager] isReadableFileAtPath:path]) {
        NSData* data = [NSData dataWithContentsOfFile:path];
        NSString *string = [[NSString alloc] initWithBytes:[data bytes]
                                                     length:[data length]
                                                   encoding:NSUTF8StringEncoding];
        [self parseFile:string];
    } else {
        DebugLog(@"Cannot read: %@", path);
    }
 */
}

- (void)parseFile:(NSString *)contents
{
    NSString *delimiter = @"\n";
    NSArray *items = [contents componentsSeparatedByString:delimiter];
    int i = 0;
    for (NSString *line in items) {
        if (i == 0) {
//            [self parseMapLine:line];
            i++;
        }
        if ([[line trim]length] > 1) {
            NSString *trim = [[line trim] substringFromIndex:1];
            if ([self isTeamLine:trim]) {
                [self setTeamForLine:trim];
            } else if ([self isTeamSetupLine:trim]) {
                [self findTeamForLine:trim];
            } else if ([self isValidLine:trim]) {
                NSRange endRange;
                if (LFIRE) {
                    endRange = [trim rangeOfString:@"Timelimit Hit"];
                } else {
                    endRange = [trim rangeOfString:@"Defense MVP"];
                }
                if (endRange.location != NSNotFound) {
                    break;
                } else {
//                    DebugLog(@"trim: %@", trim);
                    [self eventType:trim];
                }
            }
        }
    }
//    DebugLog(@"blueTeam: %@", self.blueTeam);
//    DebugLog(@"redTeam: %@", self.redTeam);
}

- (void)parseMapLine:(NSString *)line
{
    NSRange mapRange = [line rangeOfString:@"lmctf"];
    if (mapRange.location != NSNotFound) {
        NSString *first = [line substringFromIndex:mapRange.location + mapRange.length + 1];
        NSRange range = [first rangeOfString:@""];
        if (range.location != NSNotFound)
            self.mapName = [first substringToIndex:range.location];
        DebugLog(@"mapName: %@", self.mapName);
    }
}

- (void)setTeamForLine:(NSString *)line
{
    NSRange range = [line rangeOfString:@"is now on the"];
    NSString *name = [[line substringToIndex:range.location] trim];
    NSString *teamName = [[line substringFromIndex:(range.location + range.length)] trim];
    Player *p = [self findOrCreatePlayer:name];
    BOOL addToTeam = YES;
    if ([teamName isEqualToString:@"red team."]) {
//        for (Player *player in self.redTeam.players) {
//            if ([player.name isEqualToString:p.name]) {
//                addToTeam = NO;
//                break;
//            }
//        }
//        if (addToTeam) {
            DebugLog(@"Setting team to red for : %@", p.name);
            [self addPlayer:p toRedTeam:YES];
//        }
    } else {
//        for (Player *player in self.blueTeam.players) {
//            if ([player.name isEqualToString:p.name]) {
//                addToTeam = NO;
//                break;
//            }
//        }
//        if (addToTeam) {
            [self addPlayer:p toRedTeam:NO];
            DebugLog(@"Setting team to blue for : %@", p.name);
//        }
    }
}

- (void)findTeamForLine:(NSString *)line
{
    NSArray *parts = [line componentsSeparatedByString:@" "];
    for (NSString *str in parts) {
//        DebugLog(@"str: %@", str);
        NSRange range;
        if (LFIRE) {
            range = [str rangeOfString:@"/ctf_"];
        } else {
            range = [str rangeOfString:@"/rb-"];
        }
        if (range.location != NSNotFound) {
            NSArray *nameArray = [str componentsSeparatedByString:@""];
            for (NSString *n in nameArray) {
                NSRange slashRange = [n rangeOfString:@"\\"];
                if (slashRange.location != NSNotFound) {
                    NSString *name = [[n substringFromIndex:0] substringToIndex:slashRange.location];
                    DebugLog(@"name: %@", name);
                    if ([self isValidLine:name]) {
//                        DebugLog(@"Adding player: %@", name);
                        Player *p = [self findOrCreatePlayer:name];
//                        if ([self.playerArray containsObject:p]) {
//                            [self removePlayer:p];
//                        }
                        NSRange colorRange = [n rangeOfString:@"rb-"];
                        if (colorRange.location != NSNotFound) {
                            NSString *letter = [n substringFromIndex:colorRange.location+3];
                            letter = [letter substringToIndex:1];
                            if ([letter isEqualToString:@"b"]) {
                                [self addPlayer:p toRedTeam:NO];
                                DebugLog(@"Adding player: %@ to BLUE", name);
                            } else {
                                [self addPlayer:p toRedTeam:YES];
                                DebugLog(@"Adding player: %@ to RED", name);
                            }
                        }
                    }
                }
            }
        }
    }
}

- (void)removePlayer:(Player *)p fromRedTeam:(BOOL)redTeam
{
    if (redTeam) {
        for (int i = 0; i < [self.redTeam.players count]; i++) {
            Player *player = self.redTeam.players[i];
            DebugLog(@"p.name: [%@], player.name: [%@]", p.name, player.name);
            if ([p.name isEqualToString:player.name]) {
                [self.redTeam.players removeObjectAtIndex:i];
                DebugLog(@"Removing %@ from red team", p.name);
                break;
            }
        }
    } else {
        for (int i = 0; i < [self.blueTeam.players count]; i++) {
            Player *player = self.blueTeam.players[i];
            DebugLog(@"p.name: [%@], player.name: [%@]", p.name, player.name);
            if ([p.name isEqualToString:player.name]) {
                [self.blueTeam.players removeObjectAtIndex:i];
                DebugLog(@"Removing %@ from blue team", p.name);
                break;
            }
        }
    }
    DebugLog(@"red team players: %d", self.redTeam.players.count);
    DebugLog(@"blue team players: %d", self.blueTeam.players.count);
}

- (void)addPlayer:(Player *)p toRedTeam:(BOOL)redTeam
{
    DebugLog(@"player: %@", p);
    if (redTeam) {
        p.team = self.redTeam;
        p.isRedTeam = YES;
        for (Player *player in self.redTeam.players) {
            if ([player.name isEqualToString:p.name]) {
                DebugLog(@"Not adding %@ to red team", p.name);
                return;
            }
        }
        DebugLog(@"Adding %@ to red team", p.name);
        [self removePlayer:p fromRedTeam:NO];
        [self.redTeam.players addObject:p];
    } else {
        p.team = self.blueTeam;
        p.isRedTeam = NO;
        for (Player *player in self.blueTeam.players) {
            if ([player.name isEqualToString:p.name]) {
                DebugLog(@"Not adding %@ to blue team", p.name);
                return;
            }
        }
        DebugLog(@"Adding %@ to blue team", p.name);
        [self removePlayer:p fromRedTeam:YES];
        [self.blueTeam.players addObject:p];
    }
}

- (void)eventType:(NSString *)event
{
    for (NSString *phrase in self.phraseArray) {
        NSRange range = [event rangeOfString:phrase options:NSCaseInsensitiveSearch];
        if (range.location != NSNotFound) {
            [self createEntryForPhrase:phrase andLine:event];
            break;
        }
    }
}

- (void)createEntryForPhrase:(NSString *)phrase andLine:(NSString *)line
{
//    DebugLog(@"line: %@", line);
    QuakeLogEvent eventType = EventTypeIgnore;
    NSRange range = [line rangeOfString:phrase];
    NSString *victim = nil;
    NSString *killer = nil;
    if ([phrase isEqualToString:SSG_KILL]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:@"'s"];
        eventType = EventTypeKillSuperShotgun;
        previousKiller = killer;
    } else if ([phrase isEqualToString:ROCKET_KILL_1] || [phrase isEqualToString:ROCKET_KILL_2]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:@"'s"];
        eventType = EventTypeKillRocketLauncher;
        previousKiller = killer;
    } else if ([phrase isEqualToString:RAILGUN_KILL]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:nil];
        eventType = EventTypeKillRailgun;
        previousKiller = killer;
    } else if ([phrase isEqualToString:GRENADE_LAUNCHER_KILL_1] || [phrase isEqualToString:GRENADE_LAUNCHER_KILL_2]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:@"'s"];
        eventType = EventTypeKillGrenadeLauncher;
        previousKiller = killer;
    } else if ([phrase isEqualToString:CHAINGUN_KILL]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:@"'s"];
        eventType = EventTypeKillChaingun;
        previousKiller = killer;
    } else if ([phrase isEqualToString:HYPERBLASTER_KILL]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:@"'s"];
        eventType = EventTypeKillHyperBlaster;
        previousKiller = killer;
    } else if ([phrase isEqualToString:TELEFRAG_KILL]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:@"'s"];
        eventType = EventTypeKillTelefrag;
        previousKiller = killer;
    } else if ([phrase isEqualToString:MACHINEGUN_KILL]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:nil];
        eventType = EventTypeKillMachinegun;
        previousKiller = killer;
    } else if ([phrase isEqualToString:BLASTER_KILL]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:nil];
        eventType = EventTypeKillBlaster;
        previousKiller = killer;
    } else if ([phrase isEqualToString:GRENADE_KILL_1] || [phrase isEqualToString:GRENADE_KILL_2] || [phrase isEqualToString:GRENADE_KILL_3]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:@"'s"];
        eventType = EventTypeKillGrenade;
        previousKiller = killer;
    } else if ([phrase isEqualToString:SHOTGUN_KILL]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:nil];
        eventType = EventTypeKillShotgun;
        previousKiller = killer;
    } else if ([phrase isEqualToString:BFG_KILL_1] || [phrase isEqualToString:BFG_KILL_2] || [phrase isEqualToString:BFG_KILL_3]) {
        victim = [self parseFirstPlayer:line withRange:range];
        killer = [self parseSecondPlayer:line withRange:range andPhrase:@"'s"];
        eventType = EventTypeKillBFG;
        previousKiller = killer;
    } else {
        killer = [self parseFirstPlayer:line withRange:range];
        if ([phrase isEqualToString:FLAG_STEAL] || [phrase isEqualToString:FLAG_STEAL_LFIRE]) {
            eventType = EventTypeSteal;
        } else if ([phrase isEqualToString:FLAG_CAPTURE]) {
            eventType = EventTypeCapture;
        } else if ([phrase isEqualToString:ASSIST] || [phrase isEqualToString:ASSIST_DEFEND] || [phrase isEqualToString:ASSIST_RETURN]) {
            NSRange defendRange = [line rangeOfString:@"defending"];
            NSRange returnRange = [line rangeOfString:@"returning"];
            if (defendRange.location != NSNotFound) {
                eventType = EventTypeAssistDefend;
            } else if (returnRange.location != NSNotFound) {
                eventType = EventTypeAssistReturn;
            } else {
                eventType = EventTypeAssistKill;
            }
        } else if ([phrase isEqualToString:SUICIDE_DEFAULT] || [phrase isEqualToString:SUICIDE_BFG]
                                                            || [phrase isEqualToString:SUICIDE_GRENADE_1]
                                                            || [phrase isEqualToString:SUICIDE_GRENADE_2]
                                                            || [phrase isEqualToString:SUICIDE_KILL]
                                                            || [phrase isEqualToString:SUICIDE_ROCKET]) {
            eventType = EventTypeSuicide;
        } else if ([phrase isEqualToString:EFC_KILL] || [phrase isEqualToString:ASSIST_FRAG]) {
            eventType = EventTypeEFCKill;
        } else if ([phrase isEqualToString:FLAG_DROP]) {
            eventType = EventTypeDrop;
            if (LFIRE) {
                [self trackEventType:EventTypeEFCKill forActivePlayer:previousKiller andPassivePlayer:victim];
            }
        } else if ([phrase isEqualToString:DEFEND]) {
            NSRange aggRange = [line rangeOfString:@"aggressive enemy"];
            NSRange fcRange = [line rangeOfString:@"flag carrier"];
            NSRange baseRange = [line rangeOfString:@"base"];
            if (aggRange.location != NSNotFound) {
                eventType = EventTypeDefendAggressive;
            } else if (fcRange.location != NSNotFound) {
                eventType = EventTypeDefendFC;
            } else if (baseRange.location != NSNotFound) {
                eventType = EventTypeDefendBase;
            } else {
                eventType = EventTypeDefendFlag;
            }
        } else if ([phrase isEqualToString:RETURN]) {
            eventType = EventTypeReturn;
        } else if ([phrase isEqualToString:RETURN_HELP]) {
            eventType = EventTypeReturnHelped;
        } else if ([phrase isEqualToString:DEATH_BARREL] || [phrase isEqualToString:DEATH_FALLING]
                                                        || [phrase isEqualToString:DEATH_HURT]
                                                        || [phrase isEqualToString:DEATH_LASER]
                                                        || [phrase isEqualToString:DEATH_LAVA]
                                                        || [phrase isEqualToString:DEATH_SANK]
                                                        || [phrase isEqualToString:DEATH_SLIME]
                                                        || [phrase isEqualToString:DEATH_SQUISH]
                   ) {
            eventType = EventTypeDeath;
        } else {
            return;
        }
    }
//    DebugLog(@"Killer: [%@], victim: [%@]", killer, victim);
/*
    if ([killer isEqualToString:@"nwa.b3taw0lf"]) {
        DebugLog(@"%@", line);
        kills++;
    } else if ([victim isEqualToString:@"nwa.b3taw0lf"]) {
        DebugLog(@"%@", line);
        deaths++;
    }
*/     
    [self trackEventType:eventType forActivePlayer:killer andPassivePlayer:victim];
}

- (NSString *)parseFirstPlayer:(NSString *)line withRange:(NSRange)range
{
    return [[line substringToIndex:range.location] trim];
}

- (NSString *)parseSecondPlayer:(NSString *)line withRange:(NSRange)range andPhrase:(NSString *)phrase
{
//    DebugLog(@"second player: %@", line);
    NSString *firstHalf = [line substringFromIndex:(range.location + range.length)];
    NSInteger endIndex = [firstHalf length];
    if (phrase) {
        NSRange endRange = [firstHalf rangeOfString:phrase];
        endIndex = endRange.location;
    }
//    DebugLog(@"firstHalf: %@", firstHalf);
    NSString *player = [[firstHalf substringToIndex:endIndex] trim];
    return player;
}

- (void)trackEventType:(QuakeLogEvent)type forActivePlayer:(NSString *)player1 andPassivePlayer:(NSString *)player2
{
    if (type == EventTypeIgnore) {
        return;
    }
    Player *active = [self findOrCreatePlayer:player1];
    Player *passive = nil;
    if (player2) {
        passive = [self findOrCreatePlayer:player2];
        if (!active.team) {
            DebugLog(@"No team for player: %@", active.name);
            [self findTeamForPlayer:active fromPlayer:passive];
        }
    }
    switch (type) {
        case EventTypeKillBlaster:
            active.kills++;
            passive.killed++;
            active.blaster++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillShotgun:
            active.kills++;
            passive.killed++;
            active.shotgun++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillSuperShotgun:
            active.kills++;
            passive.killed++;
            active.supershotgun++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillMachinegun:
            active.kills++;
            passive.killed++;
            active.machinegun++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillChaingun:
            active.kills++;
            passive.killed++;
            active.chaingun++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillGrenadeLauncher:
            active.kills++;
            passive.killed++;
            active.grenadelauncher++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillRocketLauncher:
            active.kills++;
            passive.killed++;
            active.rocketlauncher++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillHyperBlaster:
            active.kills++;
            passive.killed++;
            active.hyperblaster++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillRailgun:
            active.kills++;
            passive.killed++;
            active.railgun++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillBFG:
            active.kills++;
            passive.killed++;
            active.bfg++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillGrenade:
            active.kills++;
            passive.killed++;
            active.grenade++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeKillTelefrag:
            active.kills++;
            passive.killed++;
            active.telefrag++;
            [active addVictim:passive.name];
            [passive addEnemy:active.name];
            break;
        case EventTypeAssistKill:
            active.assistKill++;
            break;
        case EventTypeAssistDefend:
            active.assistDefend++;
            break;
        case EventTypeAssistReturn:
            active.assistReturn++;
            break;
        case EventTypeDefendFlag:
            active.defendsFlag++;
            break;
        case EventTypeDefendBase:
            active.defendsBase++;
            break;
        case EventTypeDefendAggressive:
            active.defendsAggressive++;
            break;
        case EventTypeDefendFC:
            active.defendsFlagCarrier++;
            break;
        case EventTypeDrop:
            active.drops++;
            break;
        case EventTypeEFCKill:
            active.efckills++;
            break;
        case EventTypeReturn:
            active.returnsFlag++;
            break;
        case EventTypeReturnHelped:
            active.returnsHelped++;
            break;
        case EventTypeSteal:
            active.steals++;
            break;
        case EventTypeCapture:
            active.caps++;
            break;
        case EventTypeDeath:
            active.deaths++;
            break;
        case EventTypeSuicide:
            active.suicides++;
            break;
            
        default:
            break;
    }
}

- (Player *)findOrCreatePlayer:(NSString *)name
{
    Player *currentPlayer = nil;
    for (Player *p in self.playerArray) {
        if ([p.name isEqualToString:name]) {
            currentPlayer = p;
            break;
        }
    }
    if (!currentPlayer) {
        currentPlayer = [[Player alloc] init];
        currentPlayer.name = name;
        [self.playerArray addObject:currentPlayer];
    }
    return currentPlayer;
}

- (void)findTeamForPlayer:(Player *)player fromPlayer:(Player *)enemyPlayer
{
    if (enemyPlayer.team) {
        if (enemyPlayer.isRedTeam) {
            player.team = self.blueTeam;
            player.isRedTeam = NO;
            [self.blueTeam.players addObject:player];
            DebugLog(@"adding player to blue team: %@", player.name);
        } else {
            player.team = self.redTeam;
            player.isRedTeam = YES;
            [self.redTeam.players addObject:player];
            DebugLog(@"adding player to red team: %@", player.name);
        }
    }
}

- (void)printPlayers
{
    // Remove observers
    NSMutableArray *activePlayers = [NSMutableArray arrayWithCapacity:[self.playerArray count]];
    for (Player *p in self.playerArray) {
        if (p.kills + p.killed > 0) {
            [activePlayers addObject:p];
        }
    }

    for (Player *player in self.redTeam.players) {
        DebugLog(@"Red Team: %@", player.name);
    }
    for (Player *player in self.blueTeam.players) {
        DebugLog(@"Blue Team: %@", player.name);
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"score" ascending:NO];
    NSArray *sortedArray = [activePlayers sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
   
    NSMutableString *output = [NSMutableString string];

    // Calculate max categories
    NSNumber *maxCaps = [self.playerArray valueForKeyPath:@"@max.caps"];
    NSNumber *maxDefends = [self.playerArray valueForKeyPath:@"@max.defends"];
    NSNumber *maxAssists = [self.playerArray valueForKeyPath:@"@max.assists"];
    NSNumber *maxReturns = [self.playerArray valueForKeyPath:@"@max.returns"];
    NSNumber *maxEFCKills = [self.playerArray valueForKeyPath:@"@max.efckills"];
    NSNumber *maxSteals = [self.playerArray valueForKeyPath:@"@max.steals"];
    NSNumber *maxDrops = [self.playerArray valueForKeyPath:@"@max.drops"];
    NSNumber *maxKills = [self.playerArray valueForKeyPath:@"@max.kills"];
    NSNumber *maxKilled = [self.playerArray valueForKeyPath:@"@max.killed"];
    NSNumber *maxSuicides = [self.playerArray valueForKeyPath:@"@max.suicides"];
    NSNumber *maxDeaths = [self.playerArray valueForKeyPath:@"@max.deaths"];
    NSNumber *maxKDRatio = [self.playerArray valueForKeyPath:@"@max.kdratio"];
    NSNumber *maxHotStreak = [self.playerArray valueForKeyPath:@"@max.maxHotStreak"];
    NSNumber *maxBlaster = [self.playerArray valueForKeyPath:@"@max.blaster"];
    NSNumber *maxShotgun = [self.playerArray valueForKeyPath:@"@max.shotgun"];
    NSNumber *maxSSG = [self.playerArray valueForKeyPath:@"@max.supershotgun"];
    NSNumber *maxMachinegun = [self.playerArray valueForKeyPath:@"@max.machinegun"];
    NSNumber *maxChaingun = [self.playerArray valueForKeyPath:@"@max.chaingun"];
    NSNumber *maxGrenadeLauncher = [self.playerArray valueForKeyPath:@"@max.grenadelauncher"];
    NSNumber *maxRocketLauncher = [self.playerArray valueForKeyPath:@"@max.rocketlauncher"];
    NSNumber *maxHyperBlaster = [self.playerArray valueForKeyPath:@"@max.hyperblaster"];
    NSNumber *maxRailgun = [self.playerArray valueForKeyPath:@"@max.railgun"];
    NSNumber *maxBFG = [self.playerArray valueForKeyPath:@"@max.bfg"];
    NSNumber *maxGrenade = [self.playerArray valueForKeyPath:@"@max.grenade"];
    NSNumber *maxTelefrag = [self.playerArray valueForKeyPath:@"@max.telefrag"];

    // CSS Styling
    [output appendFormat:@"<html lang=\"en-US\">"
                        @"<head>"
                        @"<style type=\"text/css\">"
                        @"body{background-color:#000000;background-image: url('bg.jpg');background-repeat: none;background-position: top center;}"
                        @"table#players{width: 1000px;}"
                        @"table#player{width: 320px;}"
                        @"#players, #player{font-family: Arial, Helvetica, sans-serif;border-collapse:collapse;margin-left:auto;margin-right:auto;}"
                        @"#players th, #player th{font-size: 14px;border:1px solid #000000;padding:8px 7px 8px 7px;font-size: 14px;text-align:left;background-color:#ac170c;color:#ffffff;font-weight: normal;}"
                        @"#players td, #player td{font-size: 14px;border:1px solid #000000;padding:6px 7px 6px 7px;font-size: 14px;text-align:left;background-color:#3f3f3f;color:#dadada;}"
                        @"h1{font-family: Arial, Helvetica, sans-serif;text-align:center;text-transform:uppercase;font-size: 22px;font-weight: bold;color: #ffffff;padding:40px 0px 20px 0px;}"
                        @"h2{font-family: Arial, Helvetica, sans-serif;text-align:center;text-transform:uppercase;font-size: 18px;font-weight: bold;color: #9a9a9a;}"
                        @"#players td:nth-child(odd),#player td:nth-child(odd){background:#333333;}"
                        @"</style>"
     @"</head>"];
    
    [output appendFormat:@"<body>\n"];
    [output appendFormat:@"<h1>%@</h1>\n", self.mapName];

    // Team Info
    [output appendFormat:@"<h2>Team Info</h2>\n"];
    [output appendFormat:@"<table id=\"players\"><tr><th>Team Name</th><th>Captures</th><th>Assists</th><th>Returns</th><th>Defends</th><th>EFCKills</th><th>Steals</th><th>Drops</th><th>Kills</th><th>Killed</th><th>Score</th></tr>\n"];
    [output appendFormat:@"%@\n", [self.redTeam statsLine]];
    [output appendFormat:@"%@\n", [self.blueTeam statsLine]];
    [output appendFormat:@"</table>\n"];
    [output appendFormat:@"<br /><br /><br />\n"];

    // CTF Info
    [output appendFormat:@"<h2>CTF Info</h2>\n"];
    [output appendFormat:@"<table id=\"players\"><tr><th>Name</th><th>Team Name</th><th>Captures</th><th>Assists</th><th>Returns</th><th>Defends</th><th>EFCKills</th><th>Steals</th><th>Drops</th><th>Score</th><th>Score - Caps</th></tr>\n"];
    for (Player *p in sortedArray) {
        [output appendFormat:@"<tr><td>%@</td>", [p.name fixName]];
        [output appendFormat:@"<td>%@</td>", p.teamName];
        if (p.caps == [maxCaps integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.caps];
        } else {
            [output appendFormat:@"<td>%d</td>", p.caps];
        }
        if (p.assists == [maxAssists integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.assists];
        } else {
            [output appendFormat:@"<td>%d</td>", p.assists];
        }
        if (p.returns == [maxReturns integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.returns];
        } else {
            [output appendFormat:@"<td>%d</td>", p.returns];
        }
        if (p.defends == [maxDefends integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.defends];
        } else {
            [output appendFormat:@"<td>%d</td>", p.defends];
        }
        if (p.efckills == [maxEFCKills integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.efckills];
        } else {
            [output appendFormat:@"<td>%d</td>", p.efckills];
        }
        if (p.steals == [maxSteals integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.steals];
        } else {
            [output appendFormat:@"<td>%d</td>", p.steals];
        }
        if (p.drops == [maxDrops integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.drops];
        } else {
            [output appendFormat:@"<td>%d</td>", p.drops];
        }
        [output appendFormat:@"<td>%d</td>", p.score];
        [output appendFormat:@"<td>%d</td>", p.ctfScore];
        [output appendFormat:@"</tr>"];
    }
    [output appendFormat:@"</table>\n"];
    [output appendFormat:@"<br /><br /><br />\n"];

    // Frag Info
    [output appendFormat:@"<h2>Frag Info</h2>\n"];
    [output appendFormat:@"<table id=\"players\"><tr><th>Name</th><th>Kills</th><th>Killed</th><th>Suicides</th><th>Deaths</th><th>KDRatio</th><th>Worst Enemy</th><th>Easiest Prey</th><th>Hot Streak Kills</th></tr>\n"];
    for (Player *p in sortedArray) {
        [output appendFormat:@"<tr><td>%@</td>", [p.name fixName]];
        if (p.kills == [maxKills integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.kills];
        } else {
            [output appendFormat:@"<td>%d</td>", p.kills];
        }
        if (p.killed == [maxKilled integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.killed];
        } else {
            [output appendFormat:@"<td>%d</td>", p.killed];
        }
        if (p.suicides == [maxSuicides integerValue] && p.suicides > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.suicides];
        } else {
            [output appendFormat:@"<td>%d</td>", p.suicides];
        }
        if (p.deaths == [maxDeaths integerValue] && p.deaths > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.deaths];
        } else {
            [output appendFormat:@"<td>%d</td>", p.deaths];
        }
        if (p.kdratio == [maxKDRatio floatValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%0.2f</td>", p.kdratio];
        } else {
            [output appendFormat:@"<td>%0.2f</td>", p.kdratio];
        }
//        [output appendFormat:@"<td>%0.2f</td>", p.kdratio];
        [output appendFormat:@"<td>%@</td>", [p worstEnemy]];
        [output appendFormat:@"<td>%@</td>", [p easiestVictim]];
        if (p.maxHotStreak == [maxHotStreak integerValue]) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.maxHotStreak];
        } else {
            [output appendFormat:@"<td>%d</td>", p.maxHotStreak];
        }
        [output appendFormat:@"</tr>"];
    }
    [output appendFormat:@"</table>\n" ];
    [output appendFormat:@"<br /><br /><br />\n"];

    // Weapons Info
    [output appendFormat:@"<h2>Weapons Info</h2>\n"];
    [output appendFormat:@"<table id=\"players\"><tr><th>Name</th><th>Blaster</th><th>Shotgun</th><th>Super Shotgun</th><th>Machine Gun</th><th>Chaingun</th><th>Grenade Launcher</th><th>Rocket Launcher</th><th>Hyper Blaster</th><th>Railgun</th><th>BFG</th><th>Grenade</th><th>Telefrag</th></tr>\n"];
    for (Player *p in sortedArray) {
        [output appendFormat:@"<tr><td>%@</td>", [p.name fixName]];
        if (p.blaster == [maxBlaster integerValue] && p.blaster > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.blaster];
        } else {
            [output appendFormat:@"<td>%d</td>", p.blaster];
        }
        if (p.shotgun == [maxShotgun integerValue] && p.shotgun > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.shotgun];
        } else {
            [output appendFormat:@"<td>%d</td>", p.shotgun];
        }
        if (p.supershotgun == [maxSSG integerValue] && p.supershotgun > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.supershotgun];
        } else {
            [output appendFormat:@"<td>%d</td>", p.supershotgun];
        }
        if (p.machinegun == [maxMachinegun integerValue] && p.machinegun > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.machinegun];
        } else {
            [output appendFormat:@"<td>%d</td>", p.machinegun];
        }
        if (p.chaingun == [maxChaingun integerValue] && p.chaingun > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.chaingun];
        } else {
            [output appendFormat:@"<td>%d</td>", p.chaingun];
        }
        if (p.grenadelauncher == [maxGrenadeLauncher integerValue] && p.grenadelauncher > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.grenadelauncher];
        } else {
            [output appendFormat:@"<td>%d</td>", p.grenadelauncher];
        }
        if (p.rocketlauncher == [maxRocketLauncher integerValue] && p.rocketlauncher > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.rocketlauncher];
        } else {
            [output appendFormat:@"<td>%d</td>", p.rocketlauncher];
        }
        if (p.hyperblaster == [maxHyperBlaster integerValue] && p.hyperblaster > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.hyperblaster];
        } else {
            [output appendFormat:@"<td>%d</td>", p.hyperblaster];
        }
        if (p.railgun == [maxRailgun integerValue] && p.railgun > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.railgun];
        } else {
            [output appendFormat:@"<td>%d</td>", p.railgun];
        }
        if (p.bfg == [maxBFG integerValue] && p.bfg > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.bfg];
        } else {
            [output appendFormat:@"<td>%d</td>", p.bfg];
        }
        if (p.grenade == [maxGrenade integerValue] && p.grenade > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.grenade];
        } else {
            [output appendFormat:@"<td>%d</td>", p.grenade];
        }
        if (p.telefrag == [maxTelefrag integerValue] && p.telefrag > 0) {
            [output appendFormat:@"<td style=\"background-color:#2a4352;\">%d</td>", p.telefrag];
        } else {
            [output appendFormat:@"<td>%d</td>", p.telefrag];
        }
        [output appendFormat:@"</tr>"];
    }
    [output appendFormat:@"</table>\n"];
    [output appendFormat:@"<br /><br /><br />\n"];
    
    // Player tables
    [output appendFormat:@"<h2>Players Info</h2>\n"];
    for (Player *p in sortedArray) {
        [output appendFormat:@"<table id=\"player\"><tr><th>%@</th></tr><tr><th>Name</th><th>Kills</th><th>Killed By</th></tr>\n", p.name];
        for (NSString *name in [p sortedKillArray]) {
            [output appendFormat:@"<tr><td>%@</td><td>%d</td><td>%d</td></tr>\n", name, [p killsForName:name], [p killedByName:name]];
        }
        [output appendFormat:@"</table>\n"];
    }
    [output appendFormat:@"<br /><br /><br />\n"];
    [output appendFormat:@"</body>\n"];
    [output appendFormat:@"</html>\n"];
    [self outputToFile:output];
}

- (void)outputToFile:(NSString *)output
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex: 0];
    NSString *fileName = [NSString stringWithFormat:@"%@-stats.html", self.mapName];
    NSString *docFile = [docDir stringByAppendingPathComponent:fileName];
    
    NSError *error;
    DebugLog(@"Writing to %@", docFile);
    [output writeToFile:docFile atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        DebugLog(@"error: %@", error);
    }
}

- (BOOL)doesEventContain:(NSString *)substring
{
    BOOL contains = NO;
    NSRange range = [substring rangeOfString:substring options:NSCaseInsensitiveSearch];
    if (range.location != NSNotFound) {
        contains = YES;
    }
    return contains;
}

- (BOOL)isValidLine:(NSString *)line
{
    BOOL isValid = YES;
    NSCharacterSet *s = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_{}[]|-.@!#$%^&*()+= '\":><~"] invertedSet];
    NSRange invalidCharRange = [line rangeOfCharacterFromSet:s];

    if (invalidCharRange.location != NSNotFound) {
        isValid = NO;
    }
    return isValid;
}

- (BOOL)isTeamLine:(NSString *)line
{
    BOOL isTeam = NO;
    NSRange teamRange = [line rangeOfString:@"is now on the" options:NSCaseInsensitiveSearch];
    if (teamRange.location != NSNotFound) {
        isTeam = YES;
    }
    return isTeam;
}

- (BOOL)isTeamSetupLine:(NSString *)line
{
    BOOL isTeam = NO;
    NSRange abbrRange;
    if (LFIRE) {
        abbrRange = [line rangeOfString:@"/ctf_"];
    } else {
        abbrRange = [line rangeOfString:@"/rb-"];
    }
    if (abbrRange.location != NSNotFound) {
        isTeam = YES;
    }
    return isTeam;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
