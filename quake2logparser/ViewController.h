//
//  ViewController.h
//  quake2logparser
//
//  Created by Michael Costa on 2/8/13.
//  Copyright (c) 2013 Michael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Player.h"

typedef enum QuakeLogEvent {
    EventTypeKillBlaster,
    EventTypeKillShotgun,
    EventTypeKillSuperShotgun,
    EventTypeKillMachinegun,
    EventTypeKillChaingun,
    EventTypeKillGrenadeLauncher,
    EventTypeKillRocketLauncher,
    EventTypeKillHyperBlaster,
    EventTypeKillRailgun,
    EventTypeKillBFG,
    EventTypeKillGrenade,
    EventTypeKillTelefrag,
    EventTypeDefendFlag,
    EventTypeDefendBase,
    EventTypeDefendAggressive,
    EventTypeDefendFC,
    EventTypeSteal,
    EventTypeAssistKill,
    EventTypeAssistDefend,
    EventTypeAssistReturn,
    EventTypeDrop,
    EventTypeEFCKill,
    EventTypeReturn,
    EventTypeReturnHelped,
    EventTypeCapture,
    EventTypeSuicide,
    EventTypeDeath,
    EventTypeIgnore
} QuakeLogEvent;

@interface ViewController : UIViewController

@property (nonatomic, strong) NSArray *phraseArray;
@property (nonatomic, strong) NSMutableArray *playerArray;
@property (nonatomic, strong) Team *blueTeam;
@property (nonatomic, strong) Team *redTeam;
@property (nonatomic, strong) NSString *mapName;

@end
