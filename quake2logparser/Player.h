//
//  Player.h
//  quake2logparser
//
//  Created by Michael Costa on 2/8/13.
//  Copyright (c) 2013 Michael Costa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Team.h"

@interface Player : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) BOOL isRedTeam;
@property (nonatomic, strong) Team *team;
@property (nonatomic, assign) NSInteger caps;
@property (nonatomic, readonly) NSInteger assists;
@property (nonatomic, assign) NSInteger assistKill;
@property (nonatomic, assign) NSInteger assistDefend;
@property (nonatomic, assign) NSInteger assistReturn;
@property (nonatomic, readonly) NSInteger returns;
@property (nonatomic, assign) NSInteger returnsFlag;
@property (nonatomic, assign) NSInteger returnsHelped;
@property (nonatomic, assign) NSInteger defendsFlag;
@property (nonatomic, assign) NSInteger defendsBase;
@property (nonatomic, assign) NSInteger defendsAggressive;
@property (nonatomic, assign) NSInteger defendsFlagCarrier;
@property (nonatomic, readonly) NSInteger defends;
@property (nonatomic, assign) NSInteger efckills;
@property (nonatomic, assign) NSInteger steals;
@property (nonatomic, assign) NSInteger drops;
@property (nonatomic, assign) NSInteger kills;
@property (nonatomic, assign) NSInteger killed;
@property (nonatomic, assign) NSInteger suicides;
@property (nonatomic, assign) NSInteger deaths;
@property (nonatomic, assign) NSInteger blaster;
@property (nonatomic, assign) NSInteger shotgun;
@property (nonatomic, assign) NSInteger supershotgun;
@property (nonatomic, assign) NSInteger machinegun;
@property (nonatomic, assign) NSInteger chaingun;
@property (nonatomic, assign) NSInteger grenadelauncher;
@property (nonatomic, assign) NSInteger rocketlauncher;
@property (nonatomic, assign) NSInteger hyperblaster;
@property (nonatomic, assign) NSInteger railgun;
@property (nonatomic, assign) NSInteger bfg;
@property (nonatomic, assign) NSInteger grenade;
@property (nonatomic, assign) NSInteger telefrag;
@property (nonatomic, readonly) NSInteger score;
@property (nonatomic, readonly) CGFloat kdratio;
@property (nonatomic, readonly) NSString *teamName;
@property (nonatomic, strong) NSMutableArray *victimList;
@property (nonatomic, strong) NSMutableArray *enemyList;
@property (nonatomic, readonly) NSInteger hotStreak;
@property (nonatomic, readonly) NSInteger maxHotStreak;
@property (nonatomic, readonly) NSInteger ctfScore;


- (NSString *)printCTFInfoLine;
- (NSString *)printKillsLine;
- (NSString *)printWeaponStats;
- (void)addVictim:(NSString *)victim;
- (void)addEnemy:(NSString *)enemy;
- (NSString *)worstEnemy;
- (NSString *)easiestVictim;
- (NSArray *)sortedKillArray;
- (NSInteger)killsForName:(NSString *)name;
- (NSInteger)killedByName:(NSString *)name;

@end
