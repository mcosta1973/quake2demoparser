//
//  AppDelegate.h
//  quake2logparser
//
//  Created by Michael Costa on 2/8/13.
//  Copyright (c) 2013 Michael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
