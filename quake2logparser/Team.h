//
//  Team.h
//  quake2logparser
//
//  Created by Michael Costa on 2/9/13.
//  Copyright (c) 2013 Michael Costa. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum TeamType {
    TeamTypeRed,
    TeamTypeBlue
} TeamType;

@interface Team : NSObject

@property (nonatomic, assign) TeamType teamColor;
@property (nonatomic, strong) NSMutableArray *players;
@property (nonatomic, readonly) NSInteger caps;
@property (nonatomic, readonly) NSInteger assists;
@property (nonatomic, readonly) NSInteger returns;
@property (nonatomic, readonly) NSInteger defends;
@property (nonatomic, readonly) NSInteger efckills;
@property (nonatomic, readonly) NSInteger steals;
@property (nonatomic, readonly) NSInteger drops;
@property (nonatomic, readonly) NSInteger kills;
@property (nonatomic, readonly) NSInteger killed;
@property (nonatomic, readonly) NSInteger score;

- (NSInteger)teamScore;
- (NSInteger)teamCaps;
- (NSString *)statsLine;

@end
