//
//  NSString+Trimming.h
//  quake2logparser
//
//  Created by Michael Costa on 2/8/13.
//  Copyright (c) 2013 Michael Costa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Trimming)

- (NSString *)trim;
- (NSString *)fixName;

@end
