//
//  Team.m
//  quake2logparser
//
//  Created by Michael Costa on 2/9/13.
//  Copyright (c) 2013 Michael Costa. All rights reserved.
//

#import "Team.h"
#import "Player.h"

@interface Team ()
{
    BOOL statsCalculated;
}

@property (nonatomic, readwrite) NSInteger caps;
@property (nonatomic, readwrite) NSInteger assists;
@property (nonatomic, readwrite) NSInteger returns;
@property (nonatomic, readwrite) NSInteger defends;
@property (nonatomic, readwrite) NSInteger efckills;
@property (nonatomic, readwrite) NSInteger steals;
@property (nonatomic, readwrite) NSInteger drops;
@property (nonatomic, readwrite) NSInteger kills;
@property (nonatomic, readwrite) NSInteger killed;
@property (nonatomic, readwrite) NSInteger score;

@end

@implementation Team

- (id)init
{
    self = [super init];
    if (self) {
        self.players = [NSMutableArray array];
        statsCalculated = NO;
    }
    return self;
}

- (NSInteger)teamScore
{
    NSInteger score = 0;
    for (Player *player in self.players) {
        score += player.score;
    }
    return score;
}

- (NSInteger)teamCaps
{
    NSInteger caps = 0;
    for (Player *player in self.players) {
        caps += player.caps;
    }
    return caps;
}

- (void)calculateStats
{
    if (!statsCalculated) {
        for (Player *player in self.players) {
            if (player.kills + player.killed > 0) {
                DebugLog(@"calculating stats for %@ for team %@", player.name, [self teamName]);
                self.caps += player.caps;
                self.assists += player.assists;
                self.returns += player.returns;
                self.defends += player.defends;
                self.efckills += player.efckills;
                self.steals += player.steals;
                self.drops += player.drops;
                self.kills += player.kills;
                self.killed += player.killed;
                self.score += player.score;
                DebugLog(@"Adding score: %d for total: %d", player.score, self.score);
            }
        }
        statsCalculated = YES;
    }
}

- (NSString *)teamName
{
    if (self.teamColor == TeamTypeBlue) {
        return @"Blue";
    } else {
        return @"Red";
    }
}

- (NSString *)statsLine
{
    [self calculateStats];
    NSString *info = [NSString stringWithFormat:@"<tr><td>%@</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td>"
                      @"<td>%d</td></tr>",
                      [self teamName],
                      self.caps,
                      self.assists,
                      self.returns,
                      self.defends,
                      self.efckills,
                      self.steals,
                      self.drops,
                      self.kills,
                      self.killed,
                      self.score
                      ];
    return info;
}

- (NSString *)description
{
    NSString *desc = @"";
    desc = [desc stringByAppendingFormat:@"Team:\n\t color: %d,"
//            "\n\t players: %@,"
            "\n\t caps: %d,"
            "\n\t score: %d,",
            self.teamColor,
//            self.players,
            [self teamCaps],
            [self teamScore]
            ];
    return desc;
}

@end
